import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import '../core/repositories/article_repository.dart';
import '../core/repositories/user_repository.dart';
import 'models/article_model.dart';
import 'repositories/firebase_user_repository.dart';
import 'repositories/firestore_article_repository.dart';
import '../ui/notifiers/auth_notifier.dart';
import '../ui/notifiers/feed_notifier.dart';

final userRepositoryImplProvider = Provider.autoDispose<UserRepository>(
    (ref) => FirebaseUserRepository(FirebaseAuth.instance));

final articleRepositoryImplProvider = Provider.autoDispose<ArticleRepository>(
    (ref) => FirestoreArticleRepository(FirebaseFirestore.instance, ref.read));

final authStateNotifierProvider = StateNotifierProvider(
    (ref) => AuthNotifier(ref.read(userRepositoryImplProvider)));

final feedStateNotifierProvider = StateNotifierProvider(
    (ref) => FeedStateNotifier(ref.read(articleRepositoryImplProvider)));

final userStreamProvider = StreamProvider<User?>(
    (ref) => ref.read(userRepositoryImplProvider).authStateChange.cast<User>());

final articleStreamProvider = StreamProvider<QuerySnapshot<Article>>(
    (ref) => ref.read(articleRepositoryImplProvider).articles);
