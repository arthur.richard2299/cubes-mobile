import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/foundation.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'article_model.freezed.dart';
part 'article_model.g.dart';

@freezed
abstract class Article implements _$Article {
  const Article._();
  const factory Article({
    String? id,
    required String title,
    required String content,
    required String author,
    String? parentId,
    String? imagePath,
    required String categoryName,
    @Default(0) int likes,
    @Default(false) bool suspended,
    @Default(false) bool validated,
    required String date,
  }) = _Article;

  factory Article.fromJson(Map<String, dynamic> json) =>
      _$ArticleFromJson(json);

  factory Article.fromDocument(DocumentSnapshot doc) {
    final data = doc.data()! as Map<String, dynamic>;
    return Article.fromJson(data).copyWith(id: doc.id);
  }

  Map<String, dynamic> toDocument() => toJson()..remove('id');
}
