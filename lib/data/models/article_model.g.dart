// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'article_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_Article _$_$_ArticleFromJson(Map<String, dynamic> json) {
  return _$_Article(
    id: json['id'] as String?,
    title: json['title'] as String,
    content: json['content'] as String,
    author: json['author'] as String,
    parentId: json['parentId'] as String?,
    imagePath: json['imagePath'] as String?,
    categoryName: json['categoryName'] as String,
    likes: json['likes'] as int? ?? 0,
    suspended: json['suspended'] as bool? ?? false,
    validated: json['validated'] as bool? ?? false,
    date: json['date'] as String,
  );
}

Map<String, dynamic> _$_$_ArticleToJson(_$_Article instance) =>
    <String, dynamic>{
      'id': instance.id,
      'title': instance.title,
      'content': instance.content,
      'author': instance.author,
      'parentId': instance.parentId,
      'imagePath': instance.imagePath,
      'categoryName': instance.categoryName,
      'likes': instance.likes,
      'suspended': instance.suspended,
      'validated': instance.validated,
      'date': instance.date,
    };
