import 'package:firebase_auth/firebase_auth.dart';
import '../../core/dto/requests.dart';
import '../../core/repositories/user_repository.dart';

class FirebaseUserRepository implements UserRepository {
  final FirebaseAuth _firebaseAuth;

  FirebaseUserRepository(this._firebaseAuth);
  Stream<User?> get authStateChange => _firebaseAuth.authStateChanges();

  @override
  Future<void> create(SigninRequestDto request) async {
    await _firebaseAuth
        .createUserWithEmailAndPassword(
          email: request.email,
          password: request.password,
        )
        .then((value) => value.user!.updateDisplayName(request.displayName));
  }

  @override
  Future<void> login(LoginRequestDto request) async {
    await _firebaseAuth.signInWithEmailAndPassword(
      email: request.email,
      password: request.password,
    );
  }

  @override
  Future<void> logout() async => await _firebaseAuth.signOut();

  @override
  Future<void> sendResetPasswordMail(String emailToSend) async =>
      _firebaseAuth.sendPasswordResetEmail(email: emailToSend);
}
