import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import '../../core/repositories/article_repository.dart';
import '../models/article_model.dart';
import '../../ui/state/filter_input.dart';

class FirestoreArticleRepository implements ArticleRepository {
  final FirebaseFirestore _firebaseFirestore;
  final Reader reader;

  FirestoreArticleRepository(this._firebaseFirestore, this.reader);

  CollectionReference<Article> get articleRef =>
      _firebaseFirestore.collection('articles').withConverter<Article>(
            fromFirestore: (snapshot, _) => Article.fromDocument(snapshot),
            toFirestore: (article, _) => article.toDocument(),
          );

  @override
  Future<void> createArticle(Article article) async {
    // TODO: implement createArticle
    throw UnimplementedError();
  }

  @override
  Future<void> dislike(Article article) async =>
      await articleRef.doc(article.id).update({'likes': article.likes - 1});

  @override
  Stream<QuerySnapshot<Article>> get articles => articleRef.snapshots();

  @override
  Future<void> like(Article article) async =>
      await articleRef.doc(article.id).update({'likes': article.likes + 1});
}
