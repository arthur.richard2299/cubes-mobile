import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'ui/views/error.dart';
import 'ui/views/home.dart';
import 'ui/views/loading.dart';
import 'ui/views/login.dart';
import 'data/dependency_injection.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  runApp(ProviderScope(child: PoireToPoire()));
}

class PoireToPoire extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'PoireToPoire',
      theme: ThemeData(
        primarySwatch: Colors.green,
      ),
      home: FutureBuilder(
        future: Firebase.initializeApp(),
        builder: (context, snapshot) {
          if (snapshot.hasError) {
            return ErrorScreen(
              snapshot.error.toString(),
              Icons.running_with_errors_sharp,
              "Erreur d'initialisation",
            );
          } else if (snapshot.connectionState == ConnectionState.done) {
            return Consumer(builder: (context, watch, child) {
              final _authState = watch(authStateNotifierProvider);
              return _authState.when(
                data: (user) => user != null ? HomeScreen() : LoginScreen(),
                loading: () => LoadingScreen(),
                error: (err, stack) => ErrorScreen(
                  "$err".substring("$err".indexOf(RegExp('([A-Z])'))),
                  Icons.block_rounded,
                  "Erreur de connexion",
                ),
              );
            });
          } else {
            return LoadingScreen();
          }
        },
      ),
    );
  }
}
