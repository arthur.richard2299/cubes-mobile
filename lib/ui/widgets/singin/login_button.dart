import 'package:flutter/material.dart';
import '../../../core/constants.dart';

class LoginButton extends StatelessWidget {
  const LoginButton({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: TextButton(
        onPressed: () => Navigator.pop(context),
        child: Stack(
          children: <Widget>[
            Center(
              child: Text(
                "Se connecter",
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 18,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
            Positioned(
              left: 0,
              right: 0,
              bottom: 0,
              child: Container(
                  margin: EdgeInsets.only(right: defaultPadding / 4),
                  height: 7,
                  color: primaryColor.withOpacity(0.2)),
            )
          ],
        ),
      ),
    );
  }
}
