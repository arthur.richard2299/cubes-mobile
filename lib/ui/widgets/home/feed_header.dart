import 'package:flutter/material.dart';
import '../../../core/constants.dart';

class FeedHeader extends StatelessWidget {
  const FeedHeader({Key? key, required this.onPress, required this.title})
      : super(key: key);

  final Function() onPress;
  final String title;
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(right: defaultPadding, left: defaultPadding),
      child: Row(
        children: [
          SectionTitle(
            textValue: title,
          ),
          Spacer(),
          MoreButton(
            press: onPress,
          )
        ],
      ),
    );
  }
}

class SectionTitle extends StatelessWidget {
  const SectionTitle({Key? key, required this.textValue}) : super(key: key);

  final String textValue;

  @override
  Widget build(BuildContext context) {
    return Container(
        height: 24,
        child: Stack(children: <Widget>[
          Padding(
            padding: const EdgeInsets.only(left: defaultPadding / 4),
            child: Text(
              textValue,
              style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
            ),
          ),
          Positioned(
            bottom: 0,
            left: 0,
            right: 0,
            child: Container(
                margin: EdgeInsets.only(right: defaultPadding / 4),
                height: 7,
                color: primaryColor.withOpacity(0.2)),
          )
        ]));
  }
}

class MoreButton extends StatelessWidget {
  MoreButton({Key? key, required this.press}) : super(key: key);

  final Function() press;
  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
      style: ElevatedButton.styleFrom(
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
        primary: primaryColor,
        elevation: 10,
      ),
      onPressed: press,
      child: Text("More", style: TextStyle(color: Colors.white)),
    );
  }
}
