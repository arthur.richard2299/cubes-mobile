import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import '../../../core/constants.dart';
import '../../../core/entities/filter.dart';
import '../../../data/models/article_model.dart';
import '../../state/filter_input.dart';
import '../../state/filter_state.dart';
import '../../views/error.dart';

import 'article_card.dart';

class Feed extends ConsumerWidget {
  final Stream<QuerySnapshot<Article>> contentStream;

  Feed(this.contentStream);
  List<Article> filterArticles(
      List<Article> articles, Filter filter, String? value) {
    switch (filter) {
      case Filter.hot:
        articles.sort((a, b) => a.likes.compareTo(b.likes));
        return articles;
      case Filter.fresh:
        articles.sort((a, b) => b.likes.compareTo(a.likes));
        articles.sort((a, b) => a.date.compareTo(b.date));
        return articles;
      case Filter.bySearch:
        return articles
            .where((article) => article.title.toLowerCase().contains(value!))
            .toList();
      case Filter.byCategory:
        return articles
            .where((article) =>
                article.categoryName.toLowerCase().contains(value!))
            .toList();
      default:
        return articles;
    }
  }

  @override
  Widget build(BuildContext context, ScopedReader watch) {
    Size size = MediaQuery.of(context).size;
    return Container(
      // margin: EdgeInsets.symmetric(vertical: ),
      height: size.width * 0.85,
      child: StreamBuilder(
          stream: contentStream,
          builder: (context, AsyncSnapshot<QuerySnapshot<Article>> snapshot) {
            if (snapshot.hasError)
              return ErrorScreen(
                "${snapshot.error}"
                    .substring("${snapshot.error}".indexOf(RegExp('([A-Z])'))),
                Icons.no_accounts_sharp,
                "Erreur de récupération de données",
              );
            else if (snapshot.connectionState == ConnectionState.active)
              return ListView(
                scrollDirection: Axis.horizontal,
                children: filterArticles(
                  snapshot.data!.docs.map((e) => e.data()).toList(),
                  watch(filterProvider).state ?? Filter.none,
                  watch(searchProvider).state,
                ).map((article) => ArticleCard(article: article)).toList(),
              );
            else {
              return Center(
                  child: CircularProgressIndicator(
                color: primaryColor,
              ));
            }
          }),
    );
  }
}
