import 'package:badges/badges.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import '../../../core/constants.dart';
import '../../../core/utils.dart';
import '../../../data/dependency_injection.dart';
import '../../../data/models/article_model.dart';
import '../../views/detail.dart';

class ArticleCard extends StatefulWidget {
  const ArticleCard({Key? key, required this.article}) : super(key: key);

  final Article article;

  @override
  _ArticleCard createState() => _ArticleCard();
}

class _ArticleCard extends State<ArticleCard> {
  bool pressed = false;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Container(
      width: size.width * 0.82,
      height: size.width * 0.7,
      padding: EdgeInsets.all(10),
      child: Column(children: <Widget>[
        Container(
          padding: EdgeInsets.all(defaultPadding / 2),
          decoration: BoxDecoration(
              borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(15),
                  topRight: Radius.circular(15),
                  bottomLeft: Radius.circular(15),
                  bottomRight: Radius.circular(15)),
              color: Colors.white,
              boxShadow: [
                BoxShadow(
                    offset: Offset(0, 10),
                    blurRadius: 20,
                    color: primaryColor.withOpacity(0.25))
              ]),
          child: Column(children: [
            Container(
              height: size.width * 0.5,
              child: widget.article.imagePath == null
                  ? Image.asset(
                      "assets/images/image_placeholder.jpeg",
                      scale: 1.0,
                      repeat: ImageRepeat.noRepeat,
                      fit: BoxFit.fitHeight,
                    )
                  : Image.network(
                      widget.article.imagePath!,
                      scale: 1.0,
                      repeat: ImageRepeat.noRepeat,
                      fit: BoxFit.fitHeight,
                    ),
            ),
            Text("${widget.article.date}",
                style: Theme.of(context).textTheme.button),
            Text(widget.article.title,
                style: Theme.of(context)
                    .textTheme
                    .caption
                    ?.copyWith(color: primaryColor, fontSize: 15)),
            Row(children: [
              IconButton(
                  icon: Icon(Icons.remove_red_eye_rounded),
                  onPressed: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) =>
                                DetailScreen(article: widget.article)));
                  }),
              Spacer(),
              Consumer(builder: (context, watch, child) {
                final feedState = watch(feedStateNotifierProvider.notifier);
                return Badge(
                  position: BadgePosition.topEnd(top: 1, end: -5),
                  animationDuration: Duration(milliseconds: 300),
                  animationType: BadgeAnimationType.scale,
                  badgeColor: pressed ? Colors.red : Colors.grey,
                  badgeContent:
                      Text("${parsedLikeCounter(widget.article.likes)}",
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 11,
                            fontWeight: FontWeight.bold,
                          )),
                  child: IconButton(
                    icon: Icon(Icons.thumb_up),
                    onPressed: () async {
                      setState(() {
                        pressed = !pressed;
                      });
                      if (pressed)
                        feedState.likeArticle(widget.article);
                      else
                        feedState.dislikeArticle(widget.article);
                    },
                  ),
                );
              }),
            ]),
          ]),
        ),
      ]),
    );
  }
}
