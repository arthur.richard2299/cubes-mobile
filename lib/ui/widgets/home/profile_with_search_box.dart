import 'package:flutter/material.dart';
import '../../../core/constants.dart';

import 'package:flutter_svg/svg.dart';

class ProfileWithSearchBox extends StatelessWidget {
  ProfileWithSearchBox(
      {Key? key,
      required this.username,
      required this.searchFunction,
      required this.profilePictureSrc})
      : super(key: key);

  final String username;
  final Function(String) searchFunction;
  final String profilePictureSrc;
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Container(
      margin: EdgeInsets.only(bottom: defaultPadding),
      height: size.height * 0.2, //20% de la taille totale de l'écran
      child: Stack(
        children: <Widget>[
          Container(
            padding: EdgeInsets.only(
                top: defaultPadding,
                left: defaultPadding,
                right: defaultPadding,
                bottom: 36 + defaultPadding),
            height: size.height * 0.2 - 27,
            decoration: BoxDecoration(
              color: primaryColor,
              borderRadius: BorderRadius.only(
                  bottomLeft: Radius.circular(36),
                  bottomRight: Radius.circular(36)),
            ),
            child: Row(
              children: <Widget>[
                Text(
                  username,
                  style: Theme.of(context)
                      .textTheme
                      .headline6
                      ?.copyWith(color: Colors.white),
                ),
                Spacer(),
                OutlinedButton(
                  onPressed: () {},
                  style: OutlinedButton.styleFrom(
                    padding: EdgeInsets.all(0),
                    shadowColor: Colors.white,
                    side: BorderSide(color: Colors.transparent, width: 0),
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(300),
                    ),
                  ),
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(300),
                    child: Image.network(profilePictureSrc),
                  ),
                ),
              ],
            ),
          ),
          Positioned(
            bottom: 0,
            left: 0,
            right: 0,
            child: SearchBox(searchFunction: searchFunction),
          ),
        ],
      ),
    );
  }
}

class SearchBox extends StatelessWidget {
  const SearchBox({Key? key, required this.searchFunction}) : super(key: key);

  final Function(String) searchFunction;
  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.center,
      margin: EdgeInsets.symmetric(horizontal: defaultPadding),
      padding: EdgeInsets.symmetric(horizontal: defaultPadding),
      height: 54,
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(20),
          boxShadow: [
            BoxShadow(
                offset: Offset(0, 10),
                blurRadius: 50,
                color: primaryColor.withOpacity(0.23)),
          ]),
      child: Row(children: <Widget>[
        Expanded(
          child: TextField(
            onChanged: searchFunction,
            decoration: InputDecoration(
                hintText: "Recherche",
                hintStyle: TextStyle(color: primaryColor.withOpacity(0.5)),
                enabledBorder: InputBorder.none,
                focusedBorder: InputBorder.none),
          ),
        ),
        SvgPicture.asset("assets/icons/search.svg",
            height: 20, width: 20, color: primaryColor.withOpacity(0.7)),
      ]),
    );
  }
}
