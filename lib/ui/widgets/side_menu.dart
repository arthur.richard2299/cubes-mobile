import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:flutter_svg/svg.dart';
import '../../core/constants.dart';
import '../../data/dependency_injection.dart';

class SideBarMenu extends ConsumerWidget {
  @override
  Widget build(BuildContext context, ScopedReader watch) {
    final authService = watch(authStateNotifierProvider.notifier);
    Size size = MediaQuery.of(context).size;
    return Drawer(
      child: Column(
        children: <Widget>[
          DrawerHeader(
            child: Center(
              child: SvgPicture.asset(
                "assets/icons/p2p.svg",
                width: size.width,
                color: Colors.white,
              ),
            ),
            decoration: BoxDecoration(
              color: primaryColor,
            ),
          ),
          ListTile(
            leading: Icon(Icons.favorite),
            title: Text('Mes favoris'),
            onTap: () async => {},
          ),
          ListTile(
            leading: Icon(Icons.settings_sharp),
            title: Text('Paramètres'),
            onTap: () async => {},
          ),
          ListTile(
            leading: Icon(Icons.exit_to_app),
            title: Text('Deconnexion'),
            onTap: () async =>
                await authService.disconnectAndReturnToLoginScreen(),
          ),
        ],
      ),
    );
  }
}
