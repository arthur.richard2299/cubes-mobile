import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import '../../../core/constants.dart';
import '../../notifiers/is_password_visible.dart';

class LoginInputField extends ConsumerWidget {
  const LoginInputField({
    Key? key,
    required this.placeholder,
    required this.onChanged,
    this.isPassword = false,
  }) : super(key: key);

  final String placeholder;
  final Function(String) onChanged;
  final bool isPassword;

  @override
  Widget build(BuildContext context, ScopedReader watch) {
    Size size = MediaQuery.of(context).size;
    final passwordVisible = watch(isPasswordVisibleProvider);
    return Container(
      margin: EdgeInsets.only(
          left: size.width * 0.02,
          right: size.width * 0.02,
          top: size.height * 0.02,
          bottom: size.height * 0.01),
      height: 50,
      padding: EdgeInsets.only(top: 4, bottom: 7, left: 12, right: 12),
      width: size.width * 0.7,
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(12),
          boxShadow: [
            BoxShadow(
                offset: Offset(0, 10),
                blurRadius: 50,
                color: textColor.withOpacity(0.7)),
          ]),
      child: Row(
        children: [
          Expanded(
            child: TextField(
              obscureText: isPassword ? !passwordVisible : false,
              onChanged: onChanged,
              decoration: InputDecoration(
                  hintText: "  $placeholder",
                  hintStyle: TextStyle(
                      fontSize: 16, color: primaryColor.withOpacity(0.5)),
                  enabledBorder: InputBorder.none,
                  focusedBorder: InputBorder.none),
            ),
          ),
          isPassword
              ? togglePasswordVisibility(context, passwordVisible)
              : Center(),
        ],
      ),
    );
  }
}

Widget togglePasswordVisibility(BuildContext context, bool isVisible) {
  if (isVisible) {
    return IconButton(
      icon: Icon(Icons.visibility),
      onPressed: () {
        context.read(isPasswordVisibleProvider.notifier).toggleVisibility();
      },
    );
  } else {
    return IconButton(
      icon: Icon(Icons.visibility_off),
      onPressed: () {
        context.read(isPasswordVisibleProvider.notifier).toggleVisibility();
      },
    );
  }
}
