import 'package:flutter/material.dart';
import '../../../core/constants.dart';

class LoginActions extends StatelessWidget {
  const LoginActions({
    Key? key,
    required this.onSubmit,
  }) : super(key: key);

  final Function() onSubmit;
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Container(
      margin: EdgeInsets.only(
          left: size.width * 0.02, right: size.width * 0.02, bottom: 5),
      padding: EdgeInsets.only(top: 4, bottom: 7, left: 12, right: 12),
      child: Row(
        children: [
          Spacer(),
          ElevatedButton.icon(
            style: ElevatedButton.styleFrom(
              primary: Colors.white,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(12)),
            ),
            label: Text(
              "Connexion",
              style: TextStyle(color: primaryColor),
            ),
            icon: Icon(
              Icons.arrow_forward_ios_sharp,
              color: primaryColor,
            ),
            onPressed: onSubmit,
          ),
        ],
      ),
    );
  }
}
