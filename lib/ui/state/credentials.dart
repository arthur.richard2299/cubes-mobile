import 'package:flutter_riverpod/flutter_riverpod.dart';

final emailProvider = StateProvider.autoDispose<String?>((ref) => null);
final passwordProvider = StateProvider.autoDispose<String?>((ref) => null);
final verifPasswordProvider = StateProvider.autoDispose<String?>((ref) => null);
final firstNameProvider = StateProvider.autoDispose<String?>((ref) => null);
final lastNameProvider = StateProvider.autoDispose<String?>((ref) => null);
