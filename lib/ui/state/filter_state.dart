import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../../core/entities/filter.dart';

final filterProvider = StateProvider.autoDispose<Filter?>((ref) => Filter.none);
