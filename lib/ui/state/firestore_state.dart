abstract class FirestoreStatus {
  const FirestoreStatus();
}

class FirestoreSuccess extends FirestoreStatus {
  const FirestoreSuccess();
}

class FirestoreFail extends FirestoreStatus {
  final String message;
  const FirestoreFail(this.message);
}

class FirestoreLoading extends FirestoreStatus {
  const FirestoreLoading();
}
