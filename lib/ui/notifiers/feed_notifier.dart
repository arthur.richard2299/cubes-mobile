import 'package:flutter_riverpod/flutter_riverpod.dart';
import '../../core/repositories/article_repository.dart';
import '../../data/models/article_model.dart';
import '../state/firestore_state.dart';

class FeedStateNotifier extends StateNotifier<FirestoreStatus> {
  final ArticleRepository _articleRepository;

  FeedStateNotifier(this._articleRepository) : super(FirestoreLoading());

  Future<void> createArticle(Article article) async {
    try {
      _articleRepository.createArticle(article);
      state = FirestoreSuccess();
    } catch (e) {
      state = FirestoreFail("$e");
    }
  }

  Future<void> likeArticle(Article article) async {
    try {
      _articleRepository.like(article);
      state = FirestoreSuccess();
    } catch (e) {
      state = FirestoreFail("$e");
    }
  }

  Future<void> dislikeArticle(Article article) async {
    try {
      await _articleRepository.dislike(article);
      state = FirestoreSuccess();
    } catch (e) {
      state = FirestoreFail("$e");
    }
  }
}
