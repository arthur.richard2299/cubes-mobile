import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import '../../core/dto/requests.dart';
import '../../core/repositories/user_repository.dart';

class AuthNotifier extends StateNotifier<AsyncValue<Stream<User>?>> {
  final UserRepository _userRepository;

  AuthNotifier(this._userRepository) : super(AsyncValue.data(null));

  Future<void> signInAccountAndGoToHomeScreen(
      String email, String password) async {
    try {
      state = AsyncValue.loading();
      _userRepository.login(LoginRequestDto(email, password));
      state = AsyncValue.data(_userRepository.authStateChange.cast<User>());
    } on FirebaseAuthException catch (e, st) {
      state = AsyncValue.error(e, st);
    }
  }

  Future<void> createAccountAndGoToHomeScreen(String email, String password,
      String verifPassword, String displayName) async {
    try {
      state = AsyncValue.loading();
      if (verifPassword == password) {
        _userRepository.create(SigninRequestDto(email, password, displayName));
      } else {
        state = AsyncValue.error(
          'Le mot de passe ne correspond pas, veuillez recommencer la procédure',
        );
      }
      state = AsyncValue.data(_userRepository.authStateChange.cast<User>());
    } on FirebaseAuthException catch (e, st) {
      state = AsyncValue.error(e, st);
    }
  }

  Future<void> disconnectAndReturnToLoginScreen() async {
    try {
      state = AsyncValue.loading();
      await _userRepository.logout();
      state = AsyncValue.data(null);
    } on FirebaseAuthException catch (e, st) {
      state = AsyncValue.error(e, st);
    }
  }

  Future<void> sendPasswordResetEmail(String email) async {
    try {
      state = AsyncValue.loading();
      await _userRepository.sendResetPasswordMail(email);
      state = AsyncValue.data(null);
    } on FirebaseAuthException catch (e, st) {
      state = AsyncValue.error(e, st);
    }
  }
}
