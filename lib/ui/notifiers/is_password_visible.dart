import 'package:flutter_riverpod/flutter_riverpod.dart';

final isPasswordVisibleProvider =
    StateNotifierProvider((ref) => PasswordVisibilityProvider());

class PasswordVisibilityProvider extends StateNotifier<bool> {
  PasswordVisibilityProvider([bool? state]) : super(false);

  void toggleVisibility() {
    state = !state;
  }
}
