import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import '../../core/constants.dart';
import '../../data/dependency_injection.dart';
import '../state/credentials.dart';
import '../widgets/login/input_field.dart';
import '../widgets/singin/login_button.dart';

class SigninScreen extends ConsumerWidget {
  @override
  Widget build(BuildContext context, ScopedReader watch) {
    final Size size = MediaQuery.of(context).size;
    final String? email = watch(emailProvider).state;
    final String? password = watch(passwordProvider).state;
    final String? verifPassword = watch(verifPasswordProvider).state;
    final String? firstName = watch(firstNameProvider).state;
    final String? lastName = watch(lastNameProvider).state;
    return Scaffold(
      backgroundColor: textColor,
      body: SingleChildScrollView(
        child: Center(
          child: Column(
            children: [
              Container(
                margin: EdgeInsets.only(
                  left: size.width * 0.10,
                  right: size.width * 0.10,
                ),
                height: size.height * 0.2,
                width: size.width,
                child: Center(
                  child: Text(
                    "Création de compte",
                    style: TextStyle(
                      color: backgroundColor,
                      fontSize: Theme.of(context).textTheme.headline5!.fontSize,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
              ),
              Container(
                margin: EdgeInsets.only(
                  left: size.width * 0.10,
                  right: size.width * 0.10,
                  bottom: size.height * 0.05,
                ),
                decoration: BoxDecoration(
                    color: primaryColor.withOpacity(0.9),
                    borderRadius: BorderRadius.all(Radius.circular(8)),
                    boxShadow: [BoxShadow(color: Colors.white)]),
                child: Column(
                  children: [
                    LoginInputField(
                      placeholder: "Nom",
                      onChanged: (value) =>
                          context.read(lastNameProvider).state = value,
                    ),
                    LoginInputField(
                      placeholder: "Prénom",
                      onChanged: (value) =>
                          context.read(firstNameProvider).state = value,
                    ),
                    LoginInputField(
                      placeholder: "Adresse e-mail",
                      onChanged: (value) =>
                          context.read(emailProvider).state = value,
                    ),
                    LoginInputField(
                      placeholder: "Mot de passe",
                      isPassword: true,
                      onChanged: (value) =>
                          context.read(passwordProvider).state = value,
                    ),
                    LoginInputField(
                      placeholder: "Confirmation du mot de passe",
                      isPassword: true,
                      onChanged: (value) =>
                          context.read(verifPasswordProvider).state = value,
                    ),
                    Container(
                      margin: EdgeInsets.only(
                          left: size.width * 0.02,
                          right: size.width * 0.02,
                          bottom: 5),
                      padding: EdgeInsets.only(
                          top: 4, bottom: 7, left: 12, right: 12),
                      child: Row(
                        children: [
                          Spacer(),
                          ElevatedButton.icon(
                            style: ElevatedButton.styleFrom(
                              primary: Colors.white,
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(12)),
                            ),
                            label: Text(
                              "S'inscrire",
                              style: TextStyle(color: primaryColor),
                            ),
                            icon: Icon(
                              Icons.arrow_forward_ios_sharp,
                              color: primaryColor,
                            ),
                            onPressed: () async {
                              if (email != null &&
                                  password != null &&
                                  verifPassword != null &&
                                  firstName != null &&
                                  lastName != null) {
                                await context
                                    .read(authStateNotifierProvider.notifier)
                                    .createAccountAndGoToHomeScreen(
                                        email,
                                        password,
                                        verifPassword,
                                        "$firstName $lastName");
                                Navigator.pop(context);
                              }
                            },
                          )
                        ],
                      ),
                    )
                  ],
                ),
              ),
              Container(
                width: size.width * 0.68,
                child: Row(
                  children: [
                    Text(
                      "Déjà inscrit ?",
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 18,
                      ),
                    ),
                    LoginButton()
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
