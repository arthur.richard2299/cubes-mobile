import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import '../../core/constants.dart';
import '../../data/dependency_injection.dart';
import '../state/credentials.dart';

import '../widgets/login/input_field.dart';

class PasswordResetScreen extends ConsumerWidget {
  void updateEmail(BuildContext context, String email) =>
      context.read(emailProvider).state = email;
  @override
  Widget build(BuildContext context, ScopedReader watch) {
    Size size = MediaQuery.of(context).size;
    final String? email = watch(emailProvider).state;
    return Scaffold(
      backgroundColor: textColor,
      body: Center(
        child: Container(
          margin: EdgeInsets.only(
            left: size.width * 0.1,
            right: size.width * 0.1,
            top: size.height * 0.2,
          ),
          child: Column(
            children: [
              LoginInputField(
                onChanged: (value) => updateEmail(context, value),
                placeholder: "Email",
              ),
              Row(
                children: [
                  ElevatedButton(
                    style: ElevatedButton.styleFrom(primary: primaryColor),
                    child: Text("Réinitialiser le mot de passe"),
                    onPressed: () async {
                      if (email != null) {
                        await context
                            .read(authStateNotifierProvider.notifier)
                            .sendPasswordResetEmail(email);
                        Navigator.pop(context);
                      }
                    },
                  ),
                  ElevatedButton(
                    style: ElevatedButton.styleFrom(primary: Colors.white),
                    onPressed: () => Navigator.pop(context),
                    child: Text(
                      "Retour",
                      style: TextStyle(color: primaryColor),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
