import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:flutter_svg/flutter_svg.dart';
import '../../core/constants.dart';
import '../../core/navigator.dart';
import '../../data/dependency_injection.dart';
import '../state/credentials.dart';
import '../widgets/login/actions.dart';
import '../widgets/login/input_field.dart';
import 'password_reset.dart';
import 'signin.dart';

class LoginScreen extends ConsumerWidget {
  @override
  Widget build(BuildContext context, ScopedReader watch) {
    final String? email = watch(emailProvider).state;
    final String? password = watch(passwordProvider).state;
    final Size size = MediaQuery.of(context).size;
    return WillPopScope(
      onWillPop: () => Future.value(false),
      child: Scaffold(
        backgroundColor: textColor,
        body: LayoutBuilder(
          builder: (BuildContext context, BoxConstraints viewportConstaints) {
            return SingleChildScrollView(
              child: ConstrainedBox(
                constraints:
                    BoxConstraints(minHeight: viewportConstaints.maxHeight),
                child: Column(
                  children: [
                    Container(
                      margin: EdgeInsets.only(
                        left: size.width * 0.10,
                        right: size.width * 0.10,
                      ),
                      height: size.height * 0.425,
                      width: size.width,
                      child: SvgPicture.asset(
                        "assets/icons/p2p.svg",
                        fit: BoxFit.cover,
                        color: primaryColor.withOpacity(0.9),
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(
                        left: size.width * 0.10,
                        right: size.width * 0.10,
                        bottom: size.height * 0.05,
                      ),
                      decoration: BoxDecoration(
                          color: primaryColor.withOpacity(0.9),
                          borderRadius: BorderRadius.all(Radius.circular(8)),
                          boxShadow: [BoxShadow(color: Colors.white)]),
                      child: Column(
                        children: [
                          Padding(
                              padding:
                                  EdgeInsets.only(top: size.height * 0.01)),
                          LoginInputField(
                            placeholder: "Email",
                            onChanged: (value) =>
                                context.read(emailProvider).state = value,
                          ),
                          LoginInputField(
                            placeholder: "Mot de passe",
                            isPassword: true,
                            onChanged: (value) =>
                                context.read(passwordProvider).state = value,
                          ),
                          LoginActions(
                            onSubmit: () async {
                              if (email != null && password != null) {
                                return await context
                                    .read(authStateNotifierProvider.notifier)
                                    .signInAccountAndGoToHomeScreen(
                                        email, password);
                              }
                            },
                          ),
                        ],
                      ),
                    ),
                    TextButton(
                      onPressed: () => goTo(context, SigninScreen()),
                      child: Stack(children: <Widget>[
                        Padding(
                          padding:
                              const EdgeInsets.only(left: defaultPadding / 4),
                          child: Text(
                            "Créer un compte",
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 22,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ),
                        Positioned(
                          bottom: 0,
                          left: 0,
                          right: 0,
                          child: Container(
                              margin:
                                  EdgeInsets.only(right: defaultPadding / 4),
                              height: 7,
                              color: primaryColor.withOpacity(0.2)),
                        )
                      ]),
                    ),
                    TextButton(
                      onPressed: () => goTo(context, PasswordResetScreen()),
                      child: Text("Mot de passe oublié",
                          style: TextStyle(color: Colors.white)),
                    ),
                  ],
                ),
              ),
            );
          },
        ),
      ),
    );
  }
}
