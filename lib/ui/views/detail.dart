import 'package:flutter/material.dart';
import '../../core/constants.dart';
import '../../data/models/article_model.dart';

import '../widgets/side_menu.dart';

class DetailScreen extends StatelessWidget {
  DetailScreen({Key? key, required this.article}) : super(key: key);

  final Article article;
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      drawer: SideBarMenu(),
      appBar: AppBar(backgroundColor: primaryColor),
      backgroundColor: backgroundColor,
      body: SingleChildScrollView(
        child: Column(
          children: [
            Container(
              margin: EdgeInsets.all(size.width * 0.1),
              decoration: BoxDecoration(boxShadow: [BoxShadow(blurRadius: 35)]),
              child: article.imagePath == null
                  ? Image.asset(
                      "assets/images/image_placeholder.jpeg",
                      scale: 1.0,
                      repeat: ImageRepeat.noRepeat,
                    )
                  : Image.network(
                      article.imagePath!,
                      scale: 1.0,
                      repeat: ImageRepeat.noRepeat,
                    ),
            ),
            Container(
              margin: EdgeInsets.all(size.width * 0.1),
              child: Text(
                article.title,
                style: TextStyle(
                    color: textColor,
                    fontSize: 30,
                    fontFamily: "monospace",
                    shadows: [
                      Shadow(
                        color: primaryColor,
                        blurRadius: 5,
                      )
                    ]),
              ),
            ),
            Container(
              margin: EdgeInsets.all(size.width * 0.1),
              child: RichText(
                textAlign: TextAlign.justify,
                text: TextSpan(
                    style: TextStyle(
                      color: textColor,
                    ),
                    text: article.content),
              ),
            ),
            Container(
              margin: EdgeInsets.all(size.width * 0.1),
              alignment: Alignment.topRight,
              child: Text(
                article.author,
                style: TextStyle(),
              ),
            ),
            Container(
              margin: EdgeInsets.all(size.width * 0.1),
              padding: EdgeInsets.all(7),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(20)),
                  color: textColor,
                  boxShadow: [BoxShadow(color: Colors.black, blurRadius: 20)]),
              height: 70,
              child: Column(
                children: [
                  Row(
                    children: [
                      Text(
                        "test",
                        style: TextStyle(color: Colors.white),
                      ),
                    ],
                  ),
                  Row(
                    children: [
                      Text(
                        "test",
                        style: TextStyle(color: Colors.white),
                      )
                    ],
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
