import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import '../../core/constants.dart';
import '../../data/dependency_injection.dart';

class ErrorScreen extends ConsumerWidget {
  final IconData _icon;
  final String _title;
  final String message;
  const ErrorScreen(
    this.message,
    this._icon,
    this._title, {
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context, watch) {
    Size size = MediaQuery.of(context).size;
    final _authStateNotifier = watch(authStateNotifierProvider.notifier);
    return Scaffold(
      backgroundColor: textColor,
      body: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(5),
          border: Border.all(style: BorderStyle.none),
        ),
        margin: EdgeInsets.only(
            left: size.width * 0.1,
            right: size.width * 0.1,
            top: size.height * 0.2,
            bottom: size.height * 0.2),
        child: Center(
          child: Column(
            children: [
              Container(
                margin: EdgeInsets.only(bottom: size.width * 0.1),
                child: Icon(
                  _icon,
                  size: 75,
                  color: Colors.red[300],
                ),
              ),
              Container(
                margin: EdgeInsets.only(bottom: size.width * 0.05),
                child: Text(
                  _title,
                  style: TextStyle(
                      color: backgroundColor,
                      fontSize: 24,
                      fontWeight: FontWeight.w700),
                ),
              ),
              Container(
                margin: EdgeInsets.only(bottom: size.width * 0.2),
                child: Text(
                  message,
                  textAlign: TextAlign.center,
                  style: TextStyle(color: backgroundColor, fontSize: 12),
                ),
              ),
              ElevatedButton(
                style: ElevatedButton.styleFrom(primary: backgroundColor),
                onPressed: () async =>
                    await _authStateNotifier.disconnectAndReturnToLoginScreen(),
                child: Text(
                  "Retour",
                  style: TextStyle(color: primaryColor),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
