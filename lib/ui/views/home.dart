import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import '../../core/constants.dart';
import '../../core/entities/filter.dart';
import '../../data/dependency_injection.dart';
import '../state/filter_input.dart';
import '../state/filter_state.dart';
import '../widgets/login/input_field.dart';

import 'error.dart';
import '../widgets/home/feed.dart';
import '../widgets/home/feed_header.dart';
import '../widgets/home/profile_with_search_box.dart';
import '../widgets/side_menu.dart';

class HomeScreen extends ConsumerWidget {
  @override
  Widget build(BuildContext context, ScopedReader watch) {
    return WillPopScope(
      onWillPop: () => Future.value(false),
      child: Scaffold(
        floatingActionButton: FloatingActionButton(
          backgroundColor: primaryColor,
          splashColor: Colors.green,
          hoverColor: Colors.green,
          child: Icon(Icons.edit),
          onPressed: () async => {},
        ),
        drawer: SideBarMenu(),
        appBar: AppBar(backgroundColor: primaryColor),
        body: StreamBuilder(
          stream: watch(userStreamProvider.stream),
          builder: (child, AsyncSnapshot<User?> snapshot) {
            if (snapshot.hasData) {
              return SingleChildScrollView(
                child: Column(
                  children: [
                    ProfileWithSearchBox(
                      profilePictureSrc: snapshot.data!.photoURL ??
                          "https://image.flaticon.com/icons/png/512/2922/2922510.png",
                      username: snapshot.data!.displayName ??
                          snapshot.data!.email ??
                          "Anonymous",
                      searchFunction: (value) {
                        if (value != "") {
                          context.read(filterProvider).state = Filter.bySearch;
                          context.read(searchProvider).state = value;
                        } else {
                          context.read(filterProvider).state = null;
                          context.read(searchProvider).state = null;
                        }
                      },
                    ),
                    LoginInputField(
                      placeholder: "Catégorie",
                      onChanged: (value) =>
                          context.read(categoryProvider).state = value,
                    ),
                    FeedHeader(
                      onPress: () {},
                      title: "Actualités",
                    ),
                    Feed(watch(articleStreamProvider.stream)),
                  ],
                ),
              );
            } else if (snapshot.hasError) {
              return ErrorScreen(
                "${snapshot.error.toString()}".substring(
                    "${snapshot.error.toString()}".indexOf(RegExp('([A-Z])'))),
                Icons.error_outline_sharp,
                "Erreur de chargement",
              );
            } else {
              return Center(
                  child: CircularProgressIndicator(
                color: primaryColor,
              ));
            }
          },
        ),
      ),
    );
  }
}
