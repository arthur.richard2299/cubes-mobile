import 'package:cloud_firestore/cloud_firestore.dart';
import '../../data/models/article_model.dart';

abstract class ArticleRepository {
  Stream<QuerySnapshot<Article>> get articles;
  Future<void> like(Article article);
  Future<void> dislike(Article article);
  Future<void> createArticle(Article article);
}
