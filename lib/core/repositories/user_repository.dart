import 'package:firebase_auth/firebase_auth.dart';
import '../dto/requests.dart';

abstract class UserRepository {
  Stream<User?> get authStateChange;
  Future<void> login(LoginRequestDto request);
  Future<void> create(SigninRequestDto request);
  Future<void> logout();
  Future<void> sendResetPasswordMail(String emailToSend);
}
