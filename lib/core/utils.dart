String parsedLikeCounter(int likes) {
  if (likes > 999 && likes < 999999) return "${(likes / 1000).round()}k";
  if (likes > 999999)
    return "${(likes / 1000000).round()}m";
  else
    return "$likes";
}
