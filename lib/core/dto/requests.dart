class LoginRequestDto {
  final String email;
  final String password;
  const LoginRequestDto(this.email, this.password);
}

class SigninRequestDto {
  final String email;
  final String password;
  final String displayName;
  const SigninRequestDto(
    this.email,
    this.password,
    this.displayName,
  );
}

class ResetPasswordRequestDto {
  final String email;
  const ResetPasswordRequestDto(this.email);
}
